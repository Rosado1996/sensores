package pm4b.facci.com.sensores;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class ActividadSensorBrujula extends AppCompatActivity implements SensorEventListener {

    TextView lblbrujula;
    SensorManager sensorManager;
    private Sensor brujula;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_sensor_brujula);

        lblbrujula = (TextView) findViewById(R.id.lblbrujula);
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        brujula = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        float x, y, z;
        x = event.values[0];
        y = event.values[1];
        z = event.values[2];
        lblbrujula.setText("");
        lblbrujula.append("\n El valor de x : " + x + " \n El valor de y : " + y + "\n El valor de z :" + z);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        sensorManager.registerListener(this, brujula, sensorManager.SENSOR_DELAY_NORMAL);
    }
}
